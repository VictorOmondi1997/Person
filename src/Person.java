public class Person {
    private String name; private int yob;
    public Person(String name, int yob){this.name=name;this.yob=yob;}
    public String toString(){return name+ " born in "+yob;}
    public static void main(String s[]){
        Person P = new Person("Angel", 1993);
        System.out.println(P);
    }
}
